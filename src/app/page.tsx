import ItemFilme from "./components/ItemFilme";
import Pesquisa from "./components/Pesquisa";
import Footer from "./components/Footer";
import Gallery from "./components/Gallery";
import ChatBot from "./components/ChatBot"; // Importação do ChatBot

async function getFilmes() {
  const response = await fetch("http://localhost:3004/filmes", { cache: 'no-store' });
  const dados = await response.json();
  return dados;
}

export interface filmeProps {
  id: number;
  marca: string;
  modelo: string;
  cor: string;
  foto: string;
  num: number;
  total: number;
  descricao: string;
}

export default async function Home() {
  const filmes = await getFilmes();
  const listaFilmes = filmes.map((filme: filmeProps) => (
    <ItemFilme key={filme.id} filme={filme} />
  ));

  const images = [
    { src: '/images/photo1.jpg', alt: 'Photo 1' },
    { src: '/images/photo2.jpg', alt: 'Photo 2' },
    { src: '/images/photo3.jpg', alt: 'Photo 3' },
    { src: '/images/photo4.jpg', alt: 'Photo 4' },
    { src: '/images/photo5.jpg', alt: 'Photo 5' },
    { src: '/images/photo6.jpg', alt: 'Photo 6' },
    { src: '/images/photo7.jpg', alt: 'Photo 7' },
    { src: '/images/photo8.jpg', alt: 'Photo 8' },
    // Adicione mais imagens conforme necessário
  ];

  return (
    <div className="flex flex-col min-h-screen">
      <div className="flex-grow max-w-7xl mx-auto">
        <Pesquisa />
        <h1 className="mt-5 mb-4 text-2xl font-bold leading-none tracking-tight text-white lg:text-3xl dark:text-white">
          Insulfilmes disponíveis para colocação:
        </h1>
        <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-5 gap-3">
          {listaFilmes}
        </div>
        <section className="mt-8 p-4 bg-gray-900 text-white rounded-lg text-center">
          <h2 className="text-xl mb-2">
            <b>Horário de Funcionamento:</b> SEG à SEX das 8:00 às 12:00, e das 14:00 às 18:00.
          </h2>
          <h2 className="text-xl mb-2">
            <b>Modos de pagamento:</b> dinheiro, cartão de débito/crédito e PIX.
          </h2>
        </section>
        {/* Galeria de Fotos */}
        <div className="mt-8">
          <h2 className="text-2xl font-bold mb-4 text-white">Trabalhos concluídos:</h2>
          <Gallery images={images} />
        </div>
      </div>
      <Footer />
      {/* ChatBot fixo no canto inferior direito */}
      <div className="fixed bottom-4 right-4">
        <ChatBot />
      </div>
    </div>
  );
}

import type { Metadata } from "next";
import "./globals.css";
import Titulo from "./components/Titulo";
import ClienteProvider from "./context/ClienteContext";

export const metadata: Metadata = {
  title: "Elegance Filmes",
  description: "Elegance Filmes. Avaliações e Vendas de Filmes",
  keywords: ["Filmes", "Avaliações de Filmes"],
  icons: {
    icon: "/favicon.ico",
  },
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="pt-br">
      <head>
        <link rel="shortcut icon" href="./logo2.png" type="image/x-icon" />
      </head>
      <body className="bg-black text-white dark:bg-black dark:text-white">
        <ClienteProvider>
          <Titulo />
          {children}
        </ClienteProvider>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/flowbite/2.3.0/flowbite.min.js"></script>
      </body>
    </html>
  );
}

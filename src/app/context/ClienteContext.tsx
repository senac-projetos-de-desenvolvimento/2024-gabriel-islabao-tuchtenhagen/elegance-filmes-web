'use client'
// clienteContext.tsx
import { ReactNode, createContext, useState, useEffect } from "react";

interface ClienteProps {
  id: number | null;
  nome: string;
}

type ClienteContextData = {
  idClienteLogado: number | null;
  nomeClienteLogado: string;
  mudaLogin: ({ id, nome }: ClienteProps) => void;
};

// cria um contexto
export const ClienteContext = createContext({} as ClienteContextData);

function ClienteProvider({ children }: { children: ReactNode }) {
  const [idClienteLogado, setIdClienteLogado] = useState<number | null>(null);
  const [nomeClienteLogado, setNomeClienteLogado] = useState<string>("");

  useEffect(() => {
    // Recuperar dados do localStorage ao carregar o componente
    const id = localStorage.getItem("idClienteLogado");
    const nome = localStorage.getItem("nomeClienteLogado");

    if (id && nome) {
      setIdClienteLogado(Number(id));
      setNomeClienteLogado(nome);
    }
  }, []);

  function mudaLogin({ id, nome }: ClienteProps) {
    setIdClienteLogado(id);
    setNomeClienteLogado(nome);

    // Armazenar dados no localStorage ao efetuar login
    localStorage.setItem("idClienteLogado", String(id));
    localStorage.setItem("nomeClienteLogado", nome);
  }

  return (
    <ClienteContext.Provider value={{ idClienteLogado, nomeClienteLogado, mudaLogin }}>
      {children}
    </ClienteContext.Provider>
  );
}

export default ClienteProvider;

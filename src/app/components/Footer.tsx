// components/Footer.tsx
import React from 'react';

const Footer: React.FC = () => {
    return (
        <footer className="bg-gray-900 text-white py-4 mt-8">
            <div className="container mx-auto text-center">
                <div className="flex flex-col items-center">
                    <div className="flex items-center mb-2">
                        <p className="mr-2">Endereço: Avenida Fernando Osório, Nº 3285, Pelotas, RS.</p>
                        <a href="https://www.instagram.com/elegancefilm" className="social-link" target="_blank" rel="noopener noreferrer">
                            <img src="/instagram.png" alt="Instagram" width="40" height="40" />
                        </a>
                    </div>
                    <p className="mb-2">Contato via Whatsapp: (53) 991100961</p>
                    </div>
                <p>&copy; {new Date().getFullYear()} Elegance Filmes. Todos os direitos reservados.</p>
            </div>
        </footer>
    );
};

export default Footer;

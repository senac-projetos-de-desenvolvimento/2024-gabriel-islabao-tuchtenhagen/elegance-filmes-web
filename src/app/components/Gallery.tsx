interface ImageProps {
  src: string;
  alt: string;
}

interface GalleryProps {
  images: ImageProps[];
}

const Gallery = ({ images }: GalleryProps) => {
  return (
    <div className="grid grid-cols-2 sm:grid-cols-3 md:grid-cols-4 gap-4 mt-4">
      {images.map((image, index) => (
        <div key={index} className="w-full h-48 overflow-hidden rounded shadow-lg">
          <img
            src={image.src}
            alt={image.alt}
            className="w-full h-full object-cover"
          />
        </div>
      ))}
    </div>
  );
};

export default Gallery;

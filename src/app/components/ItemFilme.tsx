'use client'
import { useContext } from "react"
import { filmeProps } from "../page"
import { ClienteContext } from "../context/ClienteContext"
import Estrelas from "./Estrelas"
import { FaRegComments } from "react-icons/fa6"
import { RiChatNewFill } from "react-icons/ri"

import Link from "next/link"

function ItemFilme({ filme }: { filme: filmeProps }) {
  const { idClienteLogado } = useContext(ClienteContext)

  // Função para abrir o WhatsApp
  function abrirWhatsApp() {
    // Número de telefone para o qual você deseja redirecionar
    const numero = "5553981352417"; // Substitua com o número correto

    // Monta o link do WhatsApp com o número
    const linkWhatsApp = `https://api.whatsapp.com/send?phone=${numero}`;

    // Abre o link no navegador
    window.open(linkWhatsApp, "_blank");
  }

  return (
    <div className="max-w-sm bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
      <a href="#">
        <img className="rounded-t-lg" src={filme.foto} alt="Capa do Filme" />
      </a>
      <div className="p-5">
        <a href="#">
          <h5 className="mb-2 text-xl font-bold tracking-tight text-gray-900 dark:text-white">{filme.marca}</h5>
        </a>
        <p className="mb-2 font-normal text-gray-700 dark:text-gray-400">
          {filme.modelo} - {filme.cor} 
        </p>
       
        <p className="text-sm text-gray-500 dark:text-white mb-2">
          {filme.descricao}
        </p>

        {idClienteLogado &&
          <div>
            <Estrelas soma={filme.total} num={filme.num} />
            <div className="float-end">
             
              <Link href={"/avaliar/" + filme.id}>
                <RiChatNewFill className="text-xl text-red-600 inline" style={{ cursor: 'pointer' }} />
              </Link>
            </div>
          </div>
        }

        {/* Botão de WhatsApp */}
        <button
          className="bg-green-500 hover:bg-green-600 text-white py-2 px-4 rounded mt-4"
          onClick={abrirWhatsApp}
        >
          Agendar via WhatsApp
        </button>

      </div>
    </div>
  )
}

export default ItemFilme

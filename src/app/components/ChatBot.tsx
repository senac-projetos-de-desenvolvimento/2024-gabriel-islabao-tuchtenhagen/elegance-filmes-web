"use client";
import { useState, useEffect } from "react";

interface Message {
  sender: "user" | "bot";
  text: string;
}

const ChatBot = () => {
  const [messages, setMessages] = useState<Message[]>([
    {
      sender: "bot",
      text: "Olá, essa é uma mensagem automática. Por favor, escolha uma das opções numéricas a seguir e digite o número desejado no chat:\n1- Contato\n2- Orçamento\n3- Outros\n4- Finalizar Atendimento",
    },
  ]);
  const [input, setInput] = useState("");
  const [isBotVisible, setIsBotVisible] = useState(false); // Inicialmente invisível
  const [interactionCount, setInteractionCount] = useState(0);
  const [nome, setNome] = useState(""); // Armazenar nome
  const [contato, setContato] = useState(""); // Armazenar contato

  useEffect(() => {
    const checkBusinessHours = () => {
      const now = new Date();
      const brTime = new Date(now.toLocaleString("en-US", { timeZone: "America/Sao_Paulo" }));
      const hours = brTime.getHours();
      const minutes = brTime.getMinutes();
      const day = brTime.getDay(); // 0 = Domingo, 1 = Segunda-feira, etc.

      // Horário comercial: Segunda a Sexta, das 8h às 12h e 14h às 18h
      const isBusinessHours =
        day >= 1 && day <= 5 && // Segunda a Sexta
        ((hours > 8 && hours < 12) || (hours === 8 && minutes >= 0)) ||
        ((hours > 14 && hours < 18) || (hours === 14 && minutes >= 0));

      setIsBotVisible(!isBusinessHours); // Mostra o bot fora do horário comercial
    };

    checkBusinessHours();
    const interval = setInterval(checkBusinessHours, 60000); // Verifica a cada minuto

    return () => clearInterval(interval); // Limpa o intervalo ao desmontar o componente
  }, []);

  useEffect(() => {
    const sendContactToBackend = async () => {
      if (!nome || !contato) return;

      const contactData = { nome, contato };

      try {
        const response = await fetch("http://localhost:3004/api/salvar-contato-chatbot", {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(contactData),
        });

        if (response.ok) {
          setMessages((prev) => [
            ...prev,
            { sender: "bot", text: "Contato salvo com sucesso! Retornaremos em breve." },
            {
              sender: "bot",
              text: "Caso deseje escolher outra opção, por favor, escolha: \n1- Contato\n2- Orçamento\n3- Outros\n4- Finalizar Atendimento",
            },
          ]);
          setInteractionCount(0); // Reinicia o fluxo
        } else {
          const errorMessage = await response.json();
          setMessages((prev) => [
            ...prev,
            { sender: "bot", text: `Erro ao salvar o contato: ${errorMessage.msg}` },
          ]);
        }
      } catch (error) {
        setMessages((prev) => [
          ...prev,
          { sender: "bot", text: "Erro ao salvar contato, tente novamente mais tarde." },
        ]);
      }
    };

    if (nome && contato) {
      sendContactToBackend();
    }
  }, [nome, contato]);

  const handleSendMessage = () => {
    if (!input.trim()) return;

    const userMessage: Message = { sender: "user", text: input };
    setMessages((prev) => [...prev, userMessage]);

    setTimeout(() => {
      let botReply: Message;

      if (interactionCount === 0 && ["1", "2", "3", "4"].includes(input)) {
        const optionMessages: Record<"1" | "2" | "3" | "4", string> = {
          "1": "Você escolheu a opção 1 - Contato. Deixe seu nome e contato para que possamos retornar. Por favor, envie seu nome e número de telefone (com DDD) no formato: 'Nome, Número'. Exemplo: 'Igor, 539912345678'.",
          "2": "Você escolheu a opção 2 - Orçamento. Sabendo que o valor do orçamento pode variar conforme o modelo do veículo, deixe seu nome e contato para que possamos retornar. Por favor, envie seu nome e número de telefone (com DDD) no formato: 'Nome, Número'. Exemplo: 'Igor, 539912345678'.",
          "3": "Você escolheu a opção 3 - Outros. No momento não temos nenhum atendente disponível, deixe seu nome e contato para que possamos retornar. Por favor, envie seu nome e número de telefone (com DDD) no formato: 'Nome, Número'. Exemplo: 'Igor, 539912345678'.",
          "4": "Você escolheu a opção 4 - Finalizar Atendimento. Atendimento finalizado, muito obrigado por escolher a Elegance Filmes!",
        };

        botReply = {
          sender: "bot",
          text: optionMessages[input as "1" | "2" | "3" | "4"],
        };
        setInteractionCount(1);
      } else if (interactionCount === 1) {
        const match = input.match(/^([^,]+),\s*(\d+)$/);

        if (match) {
          setNome(match[1].trim());
          setContato(match[2].trim());

          botReply = {
            sender: "bot",
            text: "Obrigado! Suas informações estão sendo processadas.",
          };
          setInteractionCount(2);
        } else {
          botReply = {
            sender: "bot",
            text: "Formato inválido. Envie no formato: 'Nome, Número'. Exemplo: 'Igor, 539912345678'.",
          };
        }
      } else {
        botReply = {
          sender: "bot",
          text: "Desculpe, não entendi. Por favor, escolha uma opção válida ou envie seus dados.",
        };
      }

      setMessages((prev) => [...prev, botReply]);
    }, 1000);

    setInput(""); // Resetar o input do usuário
  };

  if (!isBotVisible) return null;

  return (
    <div className="bg-white shadow-lg rounded-lg p-4 max-w-sm fixed bottom-4 right-4">
      <div className="h-64 overflow-y-auto border-b mb-2">
        {messages.map((message, index) => (
          <div
            key={index}
            className={`mb-2 ${message.sender === "user" ? "text-right" : "text-left"}`}
          >
            <span
              className={`inline-block px-3 py-2 rounded-lg ${message.sender === "user" ? "bg-blue-500 text-white" : "bg-gray-200 text-black"}`}
            >
              {message.text}
            </span>
          </div>
        ))}
      </div>
      <div className="flex items-center">
        <input
          type="text"
          className="flex-grow border rounded px-2 py-1 text-black"
          placeholder="Digite sua mensagem..."
          value={input}
          onChange={(e) => setInput(e.target.value)}
        />
        <button
          className="ml-2 px-3 py-1 bg-blue-500 text-white rounded"
          onClick={handleSendMessage}
        >
          Enviar
        </button>
      </div>
    </div>
  );
};

export default ChatBot;
